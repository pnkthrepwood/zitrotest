﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{

    public UnityEngine.UI.Text clockText;

    void Start()
    {
        StartCoroutine(GetRequest());
    }

    IEnumerator GetRequest()
    {
        string uri = "https://worldtimeapi.org/api/ip";

        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log("Error on api call");
            }
            else
            {
                string json = webRequest.downloadHandler.text;
                var parsedJson = JSON.Parse(json);
                string datetime = parsedJson["datetime"];
                string date = datetime.Split('T')[0];
                string time = datetime.Split('T')[1].Split('.')[0];

                clockText.text = date.Split('-')[2] + '-' + date.Split('-')[1] + '-' + date.Split('-')[0] +
                                 ' ' + time.Split(':')[0] + ':' + time.Split(':')[1];
            }
        }
    }

    public enum GameSelected
    {
        GAME1,
        GAME2,
        GAME3
    }
    public static GameSelected gameSelected;
        

    public void LoadGame1()
    {
        gameSelected = GameSelected.GAME1;
        SceneManager.LoadScene("PlayScene");
    }

    public void LoadGame2()
    {
        gameSelected = GameSelected.GAME2;
        SceneManager.LoadScene("PlayScene");
    }

    public void LoadGame3()
    {
        gameSelected = GameSelected.GAME3;
        SceneManager.LoadScene("PlayScene");
    }
}
