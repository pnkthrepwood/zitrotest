﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayController : MonoBehaviour
{

    public GameObject animation2D;
    public GameObject particleSystem;
    public GameObject shader;

    void Start()
    {
        
        if (MenuController.gameSelected == MenuController.GameSelected.GAME1)
        {
            Instantiate(animation2D);
        }

        if (MenuController.gameSelected == MenuController.GameSelected.GAME2)
        {
            Instantiate(particleSystem);
        }

        if (MenuController.gameSelected == MenuController.GameSelected.GAME3)
        {
            Instantiate(shader);
        }

    }

    void Update()
    {
        
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
