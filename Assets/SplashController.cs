﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashController : MonoBehaviour
{

    Texture2D whiteTexture;

    float loadingTimer = 0.0f;
    float totalLoadingTime = 5.0f;

    void Start()
    {
        whiteTexture = Texture2D.whiteTexture;
    }

    void Update()
    {
        loadingTimer += Time.deltaTime;

        if (loadingTimer > totalLoadingTime)
        {
            SceneManager.LoadScene("MenuScene");
        }
    }

    void OnGUI()
    {
        float progress = Mathf.Min(1.0f, loadingTimer / totalLoadingTime);

        int screenWidth = Screen.width;
        int screenHeight = Screen.height;

        float w = screenWidth * 0.5f;
        float h = screenHeight * 0.05f;

        float x = screenWidth * 0.25f;
        float y = screenHeight * 0.75f;

        GUI.DrawTexture(new Rect(x - 5, y - 5, w + 10, h + 10), whiteTexture, ScaleMode.StretchToFill, false, 0, Color.black, 0, 0);
        GUI.DrawTexture(new Rect(x, y, progress * w, h), whiteTexture, ScaleMode.StretchToFill, false, 0, Color.green, 0, 0);
    }
}
